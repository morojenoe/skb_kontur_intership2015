using NUnit.Framework;

namespace battleships
{
	[TestFixture]
	public class MapTest
	{
		[Test]
		public void put_ship_inside_map_bounds()
		{
			var map = new Map(100, 10);
			Assert.IsTrue(map.PlaceShip(new Vector(0, 0), 5, ShipDirection.Horizontal));
			Assert.IsTrue(map.PlaceShip(new Vector(95, 9), 5, ShipDirection.Horizontal));
		}

		[Test]
		public void not_put_ship_outside_map()
		{
			var map = new Map(100, 10);
			Assert.IsFalse(map.PlaceShip(new Vector(99, 9), 2, ShipDirection.Horizontal));
			Assert.IsFalse(map.PlaceShip(new Vector(99, 9), 2, ShipDirection.Vertical));
		}

		[Test]
		public void kill_ship()
		{
			var map = new Map(100, 10);
			map.PlaceShip(new Vector(0, 0), 1, ShipDirection.Horizontal);
			Assert.AreEqual(ShotEffect.Kill, map.MakeShot(new Vector(0, 0)));
			Assert.AreEqual(MapCell.DeadOrWoundedShip, map[new Vector(0, 0)]);
		}

		[Test]
		public void wound_ship()
		{
			var map = new Map(100, 10);
			map.PlaceShip(new Vector(0, 0), 2, ShipDirection.Horizontal);
			Assert.AreEqual(ShotEffect.Wound, map.MakeShot(new Vector(0, 0)));
			Assert.AreEqual(MapCell.DeadOrWoundedShip, map[new Vector(0, 0)]);
			Assert.AreEqual(MapCell.Ship, map[new Vector(1, 0)]);
		}

        [Test]
        public void shot_outside_borders()
        {
            var map = new Map(100, 10);
            map.PlaceShip(new Vector(0, 0), 2, ShipDirection.Horizontal);
            Assert.AreEqual(ShotEffect.Miss, map.MakeShot(new Vector(200, 200)));
        }
	}
}
