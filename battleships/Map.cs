﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace battleships
{
    public enum MapCell
    {
        Empty,

        Ship,

        DeadOrWoundedShip,

        Miss
    }

    public enum ShipDirection
    {
        Horizontal,

        Vertical
    }

	public enum ShotEffect
	{
		
		Miss,
		
		Wound,
		
		Kill
	}

	public class Ship
	{
        private HashSet<Vector> aliveCells;
        public bool Alive { get { return aliveCells.Any(); } }
        public Vector Location { get; private set; }
        public int Size { get; private set; }
        public ShipDirection Direction { get; private set; }
        
		public Ship(Vector location, int size, ShipDirection direction)
		{
			Location = location;
			Size = size;
			Direction = direction;
			aliveCells = new HashSet<Vector>(GetShipCells());
		}

	    public bool RemoveCell(Vector cell)
	    {
	        return aliveCells.Remove(cell);
	    }

		public List<Vector> GetShipCells()
		{
		    if (Direction == ShipDirection.Horizontal)
		        return Vector.Rect(Location.X, Location.Y, Size, 1).ToList();
            return Vector.Rect(Location.X, Location.Y, 1, Size).ToList();
		}
	}

	public class Map
	{
		private MapCell[,] mapCells;
	    public Ship[,] ShipsMap { get; private set; }
	    public List<Ship> Ships { get; private set; }
	    public int Width { get; private set; }
	    public int Height { get; private set; }

		public Map(int width, int height)
		{
			Width = width;
			Height = height;
			mapCells = new MapCell[width, height];
            ShipsMap = new Ship[width, height];
            Ships = new List<Ship>();
		}

		public MapCell this[Vector p]
		{
			get
			{
				return InsideBorder(p) ? mapCells[p.X, p.Y] : MapCell.Empty;
			}
			private set
			{
				if (!InsideBorder(p))
					throw new IndexOutOfRangeException(p + " is not in the map borders");
				mapCells[p.X, p.Y] = value;
			}
		}

        private bool UnoccupiedCells(IEnumerable<Vector> cells)
        {
            return cells.All(cell => this[cell] == MapCell.Empty);
        }

	    private bool CanPlaceShip(List<Vector> shipCells)
	    {
            return shipCells.All(InsideBorder) && UnoccupiedCells(shipCells.SelectMany(GetNearCells));
	    }

	    public bool PlaceShip(Vector location, int size, ShipDirection direction)
		{
			var ship = new Ship(location, size, direction);
			var shipCells = ship.GetShipCells();

			if (!CanPlaceShip(shipCells)) return false;

            shipCells.ForEach(cell => this[cell] = MapCell.Ship);
            shipCells.ForEach(cell => ShipsMap[cell.X, cell.Y] = ship);
            Ships.Add(ship);

			return true;
		}

		public ShotEffect MakeShot(Vector target)
		{
			var hit = this[target] == MapCell.Ship;
			
			if (hit)
			{
				var ship = ShipsMap[target.X, target.Y];
				ship.RemoveCell(target);
				this[target] = MapCell.DeadOrWoundedShip;
				return ship.Alive ? ShotEffect.Wound : ShotEffect.Kill;
			}

			if (InsideBorder(target) && this[target] == MapCell.Empty) this[target] = MapCell.Miss;
			return ShotEffect.Miss;
		}

		public IEnumerable<Vector> GetNearCells(Vector cell)
		{
		    return Vector.Rect(cell.X - 1, cell.Y - 1, 3, 3).Where(InsideBorder);
		}

	    private bool InsideBorder(Vector p)
		{
		    return p.X >= 0 && p.X < Width && p.Y >= 0 && p.Y < Height;
		}

	    public bool HasAliveShips()
		{
		    return Ships.Any(s => s.Alive);
		}
	}
}
