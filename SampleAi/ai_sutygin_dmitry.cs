﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SampleAi
{
    class Programm
    {
        public static void Main()
        {
            try
            {
                var solver = new Solver();
                solver.Solve();
            }
            catch {}
        }
    }

    class ShotInfo
    {
        public ShotEffect Hit;
        public Vector Target;
    }

    class Solver
    {
        private Map map;
        private Random random;
        private ShotInfo lastShotInfo;
        private string lastInputLine;

        public void Solve()
        {
            lastShotInfo = new ShotInfo();
            random = new Random();
            lastInputLine = Console.ReadLine();
            while (lastInputLine != null)
            {
                InitializeMap(lastInputLine);
                PlayGame();
            }
        }

        private void PlayGame()
        {
            while (!GameIsOver())
            {
                var shipCell = FindShipCell();
                var shipCells = GetShipCells(shipCell);
                MarkShipOnMap(shipCells);
            }
        }

        private void MarkCellsNearShip(List<Vector> shipCells)
        {
            var nearCells = shipCells.SelectMany(map.GetNineNearestCells);
            nearCells = nearCells.Where(map.InsideMapBorder);
            nearCells = nearCells.Except(shipCells);
            nearCells.ToList().ForEach(cell => map[cell] = MapCell.Miss);
        }

        private void MarkShipOnMap(List<Vector> shipCells)
        {
            shipCells = shipCells.Distinct().ToList();
            map.RemoveShip(shipCells.Count);
            shipCells.ForEach(cell => map[cell] = MapCell.DeadOrWoundedShip);
            MarkCellsNearShip(shipCells);
        }

        private bool HasShipInCell(Vector cell)
        {
            if (!map.InsideMapBorder(cell)) return false;
            if (map[cell] != MapCell.Empty)
                return map[cell] == MapCell.DeadOrWoundedShip;
            var shotEffect = MakeShot(cell);
            return shotEffect != ShotEffect.Miss;
        }

        private Vector GetShipDirection(Vector someShipCell)
        {
            var directions = new Vector[] { new Vector(1, 0), new Vector(0, 1), new Vector(-1, 0), new Vector(0, -1) };
            var direction = directions[random.Next(directions.Length)];
            while (!HasShipInCell(someShipCell.Add(direction)))
                direction = directions[random.Next(directions.Length)];
            return direction;
        }

        private List<Vector> GetShipCellsOnDirection(Vector someShipCell, Vector direction)
        {
            var shipCells = new List<Vector>();
            var cell = someShipCell;

            while (lastShotInfo.Hit != ShotEffect.Kill && HasShipInCell(cell))
            {
                shipCells.Add(cell);
                cell = cell.Add(direction);
            }

            return shipCells;
        }
        
        private List<Vector> GetShipCells(Vector shipCell)
        {
            var shipCells = new List<Vector> {shipCell};
            
            if (lastShotInfo.Hit == ShotEffect.Kill)
                return shipCells;

            var direction = GetShipDirection(shipCell);
            shipCells.Add(shipCell.Add(direction));
            shipCells = shipCells.Concat(GetShipCellsOnDirection(shipCell, direction)).ToList();
            direction = direction.Mult(-1);
            shipCells = shipCells.Concat(GetShipCellsOnDirection(shipCell, direction)).ToList();

            return shipCells;
        }

        private Vector FindShipCell()
        {
            List<Vector> unoccupiedCells;
            do
            {
                unoccupiedCells = map.GetUnoccupiedCells().ToList();
                var maxNumberOfShips =
                    unoccupiedCells.Select(cell => map.CountNumberOfShipsThroughCell(cell)).Max();
                unoccupiedCells =
                    unoccupiedCells.Where(
                        cell => maxNumberOfShips - map.CountNumberOfShipsThroughCell(cell) <= 0).ToList();
            } while (MakeShot(unoccupiedCells[random.Next(unoccupiedCells.Count)]) == ShotEffect.Miss);
            return lastShotInfo.Target;
        }

        private ShotEffect MakeShot(Vector target)
        {
            Console.WriteLine("{0} {1}", target.X, target.Y);
            return ReceiveShotInfo(target);
        }

        private ShotEffect ReceiveShotInfo(Vector target)
        {
            lastInputLine = Console.ReadLine();
            if (lastInputLine == null || lastInputLine.Substring(0, 4) == "Init")
            {
                map.ClearMap();
                lastShotInfo.Hit = ShotEffect.Kill;
                lastShotInfo.Target = target;
                return lastShotInfo.Hit;
            }
            var parts = lastInputLine.Split();
            lastShotInfo.Hit = parts[0] == "Kill"? ShotEffect.Kill : (parts[0] == "Miss"? ShotEffect.Miss : ShotEffect.Wound);
            lastShotInfo.Target = new Vector(int.Parse(parts[1]), int.Parse(parts[2]));
            map[lastShotInfo.Target] = lastShotInfo.Hit == ShotEffect.Miss ? MapCell.Miss : MapCell.DeadOrWoundedShip;
            return lastShotInfo.Hit;
        }

        private bool GameIsOver()
        {
            return !map.HasAliveShips();
        }

        private void InitializeMap(string line)
        {
            var parts = line.Split();
            var width = int.Parse(parts[1]);
            var height = int.Parse(parts[2]);
            var shipSizes = parts.Skip(3).Select(int.Parse).ToList();
            map = new Map(width, height, shipSizes);
        }
    }
    
    enum ShotEffect
    {
        Miss,

        Wound,

        Kill
    }

    enum MapCell
    {
        Empty,

        DeadOrWoundedShip,

        Miss
    }

    class Map
    {
        private MapCell[,] mapCells;
        private List<int> shipSizes;
        public int Width { get; private set; }
        public int Height { get; private set; }

        public Map(int width, int height, List<int> sizesOfShip)
        {
            Width = width;
            Height = height;
            mapCells = new MapCell[width, height];
            shipSizes = sizesOfShip;
        }

        public void RemoveShip(int shipSize)
        {
            shipSizes.Remove(shipSize);
        }

        public void ClearMap()
        {
            shipSizes.Clear();
        }

        public MapCell this[Vector p]
        {
            get { return InsideMapBorder(p) ? mapCells[p.X, p.Y] : MapCell.Empty; }
            set
            {
                if (!InsideMapBorder(p))
                    throw new IndexOutOfRangeException(p + " is not in the map borders");
                mapCells[p.X, p.Y] = value;
            }
        }

        private static IEnumerable<List<Vector>> GetShipForms(int maxShipSize)
        {
            return GetHorizontalShipForms(maxShipSize)
                .Concat(GetVerticalShipForms(maxShipSize));
        }

        private static IEnumerable<List<Vector>> GetVerticalShipForms(int shipSize)
        {
            return Enumerable.Range(0, shipSize)
                .Select(
                    maxY => (Enumerable.Range(0, shipSize).Select(y => new Vector(0, maxY - y))).ToList()
                );
        }

        private static IEnumerable<List<Vector>> GetHorizontalShipForms(int shipSize)
        {
            return Enumerable.Range(0, shipSize)
                .Select(
                    maxX => (Enumerable.Range(0, shipSize).Select(x => new Vector(maxX - x, 0))).ToList()
                );
        }

        public int CountNumberOfShipsThroughCell(Vector cell)
        {
            return shipSizes.Select(size => CountNumberOfShipsThroughCell(cell, size)).Sum();
        }

        private int CountNumberOfShipsThroughCell(Vector cell, int shipSize)
        {
            var shipForms = GetShipForms(shipSize);
            return shipForms.Count(
                ship => CanPlaceShip(ship.Select(shipCell => shipCell.Add(cell)).ToList())
                );
        }

        public bool UnoccupiedCells(IEnumerable<Vector> cells)
        {
            return cells.All(cell => this[cell] == MapCell.Empty);
        }

        public bool CanPlaceShip(List<Vector> shipCells)
        {
            return shipCells.All(InsideMapBorder) && UnoccupiedCells(shipCells);
        }

        public IEnumerable<Vector> GetNineNearestCells(Vector cell)
        {
            return Vector.Rect(cell.X - 1, cell.Y - 1, 3, 3).Where(InsideMapBorder);
        }

        public bool HasAliveShips()
        {
            return shipSizes.Count > 0;
        }

        public bool InsideMapBorder(Vector p)
        {
            return p.X >= 0 && p.X < Width && p.Y >= 0 && p.Y < Height;
        }

        public IEnumerable<Vector> GetUnoccupiedCells()
        {
            return Vector.Rect(0, 0, Width, Height).Where(cell => this[cell] == MapCell.Empty);
        }
    }


    class Vector
    {
        public int X { get; private set; }
        public int Y { get; private set; }

        public Vector(int x, int y)
        {
            X = x;
            Y = y;
        }
        
        public Vector Mult(int k)
        {
            return new Vector(k*X, k*Y);
        }

        public Vector Add(Vector v)
        {
            return new Vector(v.X + X, v.Y + Y);
        }

        public static IEnumerable<Vector> Rect(int minX, int minY, int width, int height)
        {
            return
                from x in Enumerable.Range(minX, width)
                from y in Enumerable.Range(minY, height)
                select new Vector(x, y);
        }
    }
}